package assignment3;


import assignment3.gui.Gui;

import assignment3.service.PillDispenserService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class MainClass {


    public static void main(String[] args) {
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder(MainClass.class).headless(false);
        PillDispenserService medDispenserService = springApplicationBuilder.run(args).getBean(PillDispenserService.class);
        new Gui(medDispenserService);
    }
}
