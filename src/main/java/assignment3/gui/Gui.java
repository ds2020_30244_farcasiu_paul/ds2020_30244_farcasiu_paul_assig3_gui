package assignment3.gui;

import assignment3.service.PillDispenserService;
import net.minidev.json.JSONObject;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.UUID;

public class Gui {

    private DefaultTableModel model;
    private JScrollPane sp;
    private LocalDateTime clock;
    private JLabel timerLabel;
    private PillDispenserService pillDispenserService;
    private JFrame frame;
    private JTable table;
    private JTable table_1;
    private String[] columnNames  = {
            "Medication",
            "Intake interval"
    };
    private Object[][]data = {{"ibuprofen", "12:03"}};


    public Gui(PillDispenserService pillDispenserService) {
        this.pillDispenserService=pillDispenserService;
        initialize();
        timer();
    }

    /**
     * Initialize the contents of the frame.
     */



    private void initialize() {

        List<JSONObject> medicationPlan = pillDispenserService.medicationPlans(UUID.fromString("1d2981d7-9334-4419-a092-0b381fb86cbf"));

        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(100, 100, 781, 750);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.setBounds(10, 10, 729, 126);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        timerLabel = new JLabel("00:00:00");
        timerLabel.setForeground(Color.BLACK);
        timerLabel.setFont(new Font("Times New Roman", Font.PLAIN, 28));
        timerLabel.setBounds(104, 54, 114, 42);
        panel.add(timerLabel);

        JLabel lblCurrentTime_1 = new JLabel("Time:");
        lblCurrentTime_1.setForeground(Color.BLACK);
        lblCurrentTime_1.setFont(new Font("Times New Roman", Font.PLAIN, 28));
        lblCurrentTime_1.setBounds(25, 54, 80, 42);
        panel.add(lblCurrentTime_1);

        JLabel lblNewLabel_1_1_1_1 = new JLabel("Pill Dispenser");
        lblNewLabel_1_1_1_1.setBackground(Color.WHITE);
        lblNewLabel_1_1_1_1.setBounds(264, 6, 199, 42);
        panel.add(lblNewLabel_1_1_1_1);
        lblNewLabel_1_1_1_1.setForeground(Color.BLACK);
        lblNewLabel_1_1_1_1.setFont(new Font("Times New Roman", Font.BOLD, 28));

        JPanel panel_1 = new JPanel();
        panel_1.setBackground(Color.WHITE);
        panel_1.setForeground(Color.WHITE);
        panel_1.setBounds(150, 550, 100, 50);
        frame.getContentPane().add(panel_1);
        panel_1.setLayout(null);

        table=creareTabel(medicationPlan);
        table.setFont(new Font("Times New Roman", Font.PLAIN, 16));
        table.setBounds(20, 60, 686, 350);
        sp = new JScrollPane(table);
        sp.setBounds(20, 150, 686, 350);
        frame.getContentPane().add(sp);


        JButton btnNewButton = new JButton("Take");
        btnNewButton.setFont(new Font("Times New Roman", Font.PLAIN, 16));
        btnNewButton.setBackground(Color.BLUE);
        btnNewButton.setForeground(Color.BLACK);
        btnNewButton.setBounds(0, 0, 100, 50);
        panel_1.add(btnNewButton);
        frame.setVisible(true);
    }

    private JTable creareTabel(List<JSONObject> jsonObjectList)
    {
        List<String> medicamente = new ArrayList<>();
        List<String> intervale = new ArrayList<>();

        for (int i = 0; i < jsonObjectList.size(); i++) {
            String[] s1 = jsonObjectList.get(i).getAsString("medication").split(", ");
            String[] s2 = jsonObjectList.get(i).getAsString("interval").split(", ");

            for (int k = 0; k < s1.length; k++) {
                medicamente.add(s1[k]);
                intervale.add(s2[k]);
            }

        }

        String[] columns = {"Medication", "Intake interval"};
        data = new Object[medicamente.size() + 1][2];
        for (int i = 0; i < medicamente.size(); i++) {

            data[i][0] = medicamente.get(i);
            data[i][1] = intervale.get(i);
            System.out.println(data[i][0].toString() + data[i][1].toString());
        }
        model = new DefaultTableModel(data, columns);
        table = new JTable(model);
        return table;
    }

    private void timer()
    {
        Thread timer = new Thread(() -> {
            while (true) {

                clock = java.time.LocalDateTime.now();
                timerLabel.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond());

                int Hour = 15;
                int Minute = 56;
                int Second = 30;

                if (clock.getHour() == Hour && clock.getMinute() == Minute && clock.getSecond() == Second) {
                    List<JSONObject> medicationPlan = pillDispenserService.medicationPlans(UUID.fromString("1d2981d7-9334-4419-a092-0b381fb86cbf"));
                    creareTabel(medicationPlan);
                    frame.remove(sp);
                    sp = new JScrollPane(table);
                    sp.setBounds(20, 150, 686, 350);
                    frame.getContentPane().add(sp);
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        timer.start();
    }

}