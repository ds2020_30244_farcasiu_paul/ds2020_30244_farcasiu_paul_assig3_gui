package assignment3.service;


import net.minidev.json.JSONObject;

import java.util.List;
import java.util.UUID;

public interface PillDispenserService {
    List<JSONObject> medicationPlans(UUID id);
}
