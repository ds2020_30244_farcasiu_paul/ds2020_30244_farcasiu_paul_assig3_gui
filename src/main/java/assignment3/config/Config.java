package assignment3.config;

import assignment3.service.PillDispenserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

@Configuration
public class Config {
    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
       // invoker.setServiceUrl("http://localhost:8080/pillDispenser");
        invoker.setServiceUrl("https://ds20201backpaul.herokuapp.com/pillDispenser");
        invoker.setServiceInterface(PillDispenserService.class);
        return invoker;
    }
}
